# Dev Env Setup
I'm assuming you're running on Fedora. This matters only if you're not setting up your VM manually.

## VM Specs
- 2GB RAM
- 2 vCPUs
- can access public internet
- you can access it with ssh
- Rocky Linux 9

## Walkthrough
```shell


# Create become password file (ofc don't use echo!)
touch /path/to/become/password
chmod 600 /path/to/become/password
echo "ChangeMe!" > /path/to/become/password

# Create ssh password file (ofc don't use echo!)
touch /path/to/ssh/password
chmod 600 /path/to/ssh/password
echo "ChangeMe!" > /path/to/ssh/password

# Create ssh key
ssh-keygen -t ed25519 -f /path/to/ssh/key

# setup environment variables
# You can use a different file, just don't forget to source it before trying to run playbooks
cat << EOF >> ~/.bash_profile

# Ansible overrides
export ANSIBLE_BECOME_PASSWORD_FILE=/path/to/become/password
export ANSIBLE_CONNECTION_PASSWORD_FILE=/path/to/ssh/password
export ANSIBLE_PRIVATE_KEY_FILE=/path/to/ssh/key

EOF

. ~/.bash_profile

# install required ansible modules
ansible-galaxy install -r requirements.yml

# run site_local to setup your VM
ansible-playbook site_local.yml -e 'ssh_pub_key=/path/to/ssh/key.pub'

# once that's finished, install all the stuff
ansible-playbook site.yml -e 'ssh_pub_key=/path/to/ssh/key.pub'
```

## Prod deploy
```shell
touch vault_decrypt_key
chmod 600 vault_decrypt_key
echo "Some password you store in password manager" > vault_decrypt_key

# You can find the exact name in secrets_file.yml once you decrypt it
touch ansible_ssh_key
chmod 600 ansible_ssh_key
echo "ansible ssh key" > ansible_ssh_key

# You can find the exact name in secrets_file.yml once you decrypt it
touch ansible_ssh_key.pub
chmod 600 ansible_ssh_key.pub
echo "ansible ssh key" > ansible_ssh_key.pub

ansible-playbook site.yml -i inventory_prod.yml -e @secrets_file.yml --vault-password-file vault_decrypt_key 
```
