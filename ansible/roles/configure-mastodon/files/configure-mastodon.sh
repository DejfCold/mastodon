#!/bin/bash

cd live
RAILS_ENV=production bundle exec rake db:migrate:setup
NODE_OPTIONS='--openssl-legacy-provider' RAILS_ENV=production bundle exec rake assets:precompile
